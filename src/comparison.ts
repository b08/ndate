import { NDate } from "./ndate.type";

export function earlier(date: NDate, date2: NDate): boolean {
  return date.nDate < date2.nDate;
}

export function later(date: NDate, date2: NDate): boolean {
  return date.nDate > date2.nDate;
}

export function nDateEquals(date: NDate, date2: NDate): boolean {
  return date.nDate === date2.nDate;
}

export const equal = nDateEquals;
