import { NDate } from "./ndate.type";
import { isNDate } from "./nDate";
import { NDateTime } from "./nDateTime.type";

export function toNDate(date: Date): NDate {
  if (!date || date.constructor !== Date) { return null; }
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const nDate = year * 10000 + month * 100 + day;
  return { nDate };
}

export function toDate(date: NDate): Date {
  if (!isNDate(date)) { return null; }
  const year = Math.floor(date.nDate / 10000);
  const month = Math.floor(date.nDate / 100) % 100;
  const day = date.nDate % 100;
  return new Date(year, month - 1, day);
}

export function today(): NDate {
  return toNDate(new Date());
}

export function toNDateTime(date: Date): NDateTime {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const seconds = date.getSeconds();
  const nDate = year * 10000 + month * 100 + day;
  const nTime = hour * 10000 + minute * 100 + seconds;
  return nTime + nDate * 1000000;
}

export function fromNDateTime(n: NDateTime): Date {
  const date = Math.floor(n / 1000000);
  const time = n % 1000000;

  const year = Math.floor(date / 10000);
  const month = Math.floor(date / 100) % 100;
  const day = date % 100;

  const hour = Math.floor(time / 10000);
  const minute = Math.floor(time / 100) % 100;
  const second = time % 100;
  return new Date(year, month - 1, day, hour, minute, second);
}
