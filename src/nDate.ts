import { NDate } from "./ndate.type";

export function nDate(d: number): NDate {
  return d == null ? null : { nDate: d };
}

export function isNDate(object: any): object is NDate {
  return object !== null && typeof object === "object" && typeof object["nDate"] === "number";
}
