# @b08/ndate, seeded from @b08/library-seed, library type: dry
This package serves following purposes
1. Avoid timezone conversion. I.e. solution for common "-1 day" bug. Date is stored as 8-digit number, 20191010 for instance.
   I suggest to store it in databases as number. 
2. Example on how to avoid "primitive obsession" anti-pattern. Function that receives a number will also get any other number, regardless of its meaning.
   Function receiving NDate object will give you compilation error for numbers and other arbitrary objects.

# constructor and conversions
direct constructor, supposed to be used when data is received from database. And in tests
```
import { nDate } from "@b08/ndate"

const d = nDate(20191010);
```

converters from and to js Date
```
import { toNDate, toDate } from "@b08/ndate"

const ndate = toNDate(new Date);
const date = toDate(ndate);
```

# comparison
earlier, later and equal - methods to indicate if first date is earlier than, later than or equals second date respectively

# isNDate
return true if object is NDate