import { nDate, toNDate, toDate, toNDateTime, fromNDateTime } from "../src";
import { test } from "@b08/test-runner";

test("toNDate should convert the date", async t => {
  // arrange
  const src = new Date(2019, 9, 10); // october 10th

  // act
  const result = toNDate(src);

  // assert
  t.equal(result.nDate, 20191010);
});

test("toDate should convert back to date", async t => {
  // arrange
  const src = nDate(20191010);

  // act
  const result = toDate(src);

  // assert
  t.equal(result.getTime(), new Date(2019, 9, 10).getTime());
});

test("toNDateTime should convert the date", async t => {
  // arrange
  const src = new Date(2019, 9, 10, 11, 12, 13); // october 10th

  // act
  const result = toNDateTime(src);

  // assert
  t.equal(result, 20191010111213);
});

test("fromNDateTime should convert the date", async t => {
  // arrange
  const src = 20191010111213;

  // act
  const result = fromNDateTime(src);

  // assert
  t.equal(result.getTime(), new Date(2019, 9, 10, 11, 12, 13).getTime());
});
