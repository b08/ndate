import { test } from "@b08/test-runner";
import { isNDate, nDate } from "../src";

test("isNDate should return true for NDate", async t => {
  // arrange
  const src = nDate(20191010);

  // act
  const result = isNDate(src);

  // assert
  t.true(result);
});

test("isNDate should return false for Date", async t => {
  // arrange
  const src = new Date();

  // act
  const result = isNDate(src);

  // assert
  t.false(result);
});

test("isNDate should return false for null", async t => {
  // arrange

  // act
  const result = isNDate(null);

  // assert
  t.false(result);
});

test("isNDate should return false for object with null instead of number", async t => {
  // arrange

  // act
  const result = isNDate({ nDate: null });

  // assert
  t.false(result);
});
