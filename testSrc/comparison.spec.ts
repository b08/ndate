import { earlier, nDate, later, nDateEquals, equal } from "../src";
import { test } from "@b08/test-runner";

test("earlier should return true", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191011);

  // act
  const result = earlier(d1, d2);

  // assert
  t.true(result);
});

test("earlier should return false", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191008);

  // act
  const result = earlier(d1, d2);

  // assert
  t.false(result);
});

test("later should return true", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191008);

  // act
  const result = later(d1, d2);

  // assert
  t.true(result);
});

test("later should return false", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191011);

  // act
  const result = later(d1, d2);

  // assert
  t.false(result);
});

test("nDateEquals should return true", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191010);

  // act
  const result = nDateEquals(d1, d2);

  // assert
  t.true(result);
});

test("nDateEquals should return false", async t => {
  // arrange
  const d1 = nDate(20191010);
  const d2 = nDate(20191011);

  // act
  const result = equal(d1, d2);

  // assert
  t.false(result);
});
